import java.util.Stack;

public class RemoveNodesFromLinkedList {
    public  static ListNode removeNodes(ListNode head) {
        ListNode result = null;
        Stack<ListNode> st = new Stack<>();
        ListNode temp = head;
        while(temp  != null) {
            st.push(temp);
            temp = temp.next;
        }
        while(!st.isEmpty()){
            ListNode node = st.pop();
            if(result == null) result = node;
            else if(result.val <=node.val) {
                node.next = result;
                result = node;
            }
        }
        return result;
    }
     public static void main(String[] args) {
        ListNode a = new ListNode(1);
        ListNode head = a;
        ListNode b = new ListNode(1);
        a.next = b;
        ListNode c = new ListNode(1);
        b.next = c;
        ListNode d = new ListNode(1);
        c.next = d;
        ListNode e = new ListNode(1);
        d.next = e;

        System.out.println("Before removing nodes");
        ListNode.printList(head);
        System.out.println("\nAfter removing nodes");
        ListNode.printList(removeNodes(head));
     }
}
