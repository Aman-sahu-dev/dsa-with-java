public class PathwithMaximumGold {
    static int maxGold = 0;
    public static int getMaximumGold(int[][] grid) {
        int row = grid.length, col = grid[0].length;

        for(int i = 0; i < row; i++) {
            for(int j = 0; j < col; j++) {
                if(grid[i][j] != 0 ){
                    maxGold = Math.max(maxGold ,findMaxGold(grid, i , j));
                }
            }
        }
        printMatrix(grid);
        return maxGold;
    }

    public static int findMaxGold(int[][] grid, int i , int j){
        if(i<0 || i>=grid.length || j<0 || j>=grid[0].length|| grid[i][j]==0) return 0;

        int value = grid[i][j], maxSum = 0;
        maxSum +=grid[i][j];
        grid[i][j] = 0;

        int left = findMaxGold(grid, i, j-1); //left
        int right = findMaxGold(grid, i, j+1); //right
        int up = findMaxGold(grid, i-1, j); //up
        int down = findMaxGold(grid, i+1, j); //down
        grid[i][j] = value;
        return maxSum += Math.max(Math.max(left, right), Math.max(up, down));
    }
    public static void main(String[] args) {
        int[][] grid = {{0,6,0},{5,8,7},{0,9,0}};
        printMatrix(grid);
        System.out.println(getMaximumGold(grid));

    }
    static void printMatrix(int[][] grid) {
        for(int i = 0; i < grid.length; i++){
            for(int j = 0; j <grid[0].length; j++){
                System.out.print(grid[i][j]+", ");
            }
            System.out.println();
        }
    }
}
