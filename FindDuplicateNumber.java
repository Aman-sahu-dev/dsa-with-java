import java.util.HashSet;

public class FindDuplicateNumber {
    public static int findDuplicate(int[] nums) {
        HashSet<Integer> arr = new HashSet<>();
        int result = 0;
        for(int num: nums){
            if(!arr.contains(num)){
                arr.add(num);
            }
            else{
                result = num;
            }
        }
        return result;
    }
    public static void main(String[] args) {
        int nums[] = {1,2,3,4,5,2};
        System.out.println(findDuplicate(nums));
    }
}
