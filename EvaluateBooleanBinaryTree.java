public class EvaluateBooleanBinaryTree {
    public static boolean evaluateTree(TreeNode root) {
        boolean left= true;
        boolean right= true;
        if(root.left == null && root.right == null)
            return root.val == 1?true:false;
        if(root.left!=null)
            left = evaluateTree(root.left);
        if(root.right!=null)
            right = evaluateTree(root.right);
        return root.val==2?(left||right):(left&&right);
    }
    public static void main(String[] args) {
        TreeNode root = new TreeNode(2,new TreeNode(1,null,null),new TreeNode(3,new TreeNode(0,null,null),new TreeNode(1,null,null)));

        System.out.println("result: "+ evaluateTree(root));
    }

}
