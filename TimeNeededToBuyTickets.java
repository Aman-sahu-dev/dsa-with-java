public class TimeNeededToBuyTickets {

    public static void printarr(int[] arr) {
        System.out.print("[");
        for(int ele: arr) {
            System.out.print(ele +",");
        }
        System.out.println("]");
    }
    public static int timeRequiredToBuy(int[] tickets, int k) {
        int time = 0;
        while(tickets[k]!=0){
            for(int i=0; i<tickets.length; i++){
                if(tickets[i] >0){
                    tickets[i] = tickets[i]-1;
                    time++;
                }
                if(tickets[k]==0) break;
            }
            printarr(tickets);
        }
        return time;
    }
    public static void main(String[] args) {
        int tickets[] = {84,49,5,24,70,77,87,8};
        System.out.println(timeRequiredToBuy(tickets, 3));
    }
}
