public class removeElement {
    public static int removeElements(int[] nums, int target){
        int current = 0;
        for(int i=0; i<nums.length;i++){
            if(nums[i]==target){
                continue;
            }
            else{
                nums[current]= nums[i];
                current++;
            }
        }
        // for(int i=0; i<nums.length;i++){
        //     System.out.println(nums[i]);
        // }
        return current;
    }
    public static void main(String[] args) {
        int nums[] = {1,3,2,5,6,2,8,2};
        int result = removeElements(nums, 2);

        System.out.println(result);
    }
}
