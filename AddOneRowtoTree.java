
public class AddOneRowtoTree {
    
    public static TreeNode addOneRow(TreeNode root, int val, int depth) {
        if(depth==1){
            TreeNode node = root;
            TreeNode newNode = new TreeNode(val);
            newNode.left = node;
            return newNode;
        }
        dfs(root, val, depth,1);
        return root;
    }

    static void dfs(TreeNode node,int val,int depth,int curr) {
        if(node == null) return;
        if(curr < depth-1){
            dfs(node.left,val,depth,curr+1);
            dfs(node.right,val,depth,curr+1);
        }
        if(curr == depth-1){
            TreeNode exleftNode = node.left;
            TreeNode exrightNode = node.right;

            TreeNode newleftNode = new TreeNode(val);
            TreeNode newrightNode = new TreeNode(val);

            node.left = newleftNode;
            newleftNode.left = exleftNode;
            node.right = newrightNode;
            newrightNode.right = exrightNode;
        }
    }
    public static void main(String[] args) {
        TreeNode root = new TreeNode(4,new TreeNode(2,new TreeNode(3,null,null),new TreeNode(1,null,null)),new TreeNode(6,new TreeNode(5,null,null),null));

        TreeNode root2 = new TreeNode(4,new TreeNode(2,new TreeNode(3,null,null),new TreeNode(1,null,null)),null);

        TreeNode root3 = new TreeNode(1,new TreeNode(2,new TreeNode(4,null,null),null),new TreeNode(3,null,null));

        // TreeNode result1 = addOneRow(root, 1, 2); 
        
        TreeNode result2 = addOneRow(root2, 1, 3);
        // TreeNode result3 = addOneRow(root3, 5, 4);

    }
}
