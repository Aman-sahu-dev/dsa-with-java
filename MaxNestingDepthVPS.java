import java.util.Stack;

public class MaxNestingDepthVPS {
    public static int maxDepth(String s) {
        int max =0;
        Stack<Character> stack = new Stack<>();
        if(s.length()==0) return 0;
        for(int i=0; i<s.length(); i++){
            if(s.charAt(i) == '('){
                stack.push(s.charAt(i));
                max = Math.max(max, stack.size());
                System.out.println("push: "+max);
            }
            else if(s.charAt(i) == ')'){
                stack.pop();
                max = Math.max(max, stack.size());
                System.out.println("pop: "+max);

            }
            else continue;
        }
        return max;
    }
    public static void main(String[] args) {
        // System.out.println("MaxNestingDepth : "+ maxDepth("(1+(2*3)+((8)/4))+1"));
        System.out.println("MaxNestingDepth : "+ maxDepth("(1)+((2))+(((3)))"));
    }
}
