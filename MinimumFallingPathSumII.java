public class MinimumFallingPathSumII {
    int[][] dp = new int[205][205];

    public static int solve(int[][] grid, int row, int col,MinimumFallingPathSumII m){
        int n = grid.length;
        if(row==n) return 0;
        if(m.dp[row][col]!=0) return m.dp[row][col];

        int ans = Integer.MAX_VALUE;
        for(int i=0; i<n; i++){
            if(i!=col) ans = Math.min(ans, solve(grid, row+1, i,m));
        }

        return m.dp[row][col] = ans + grid[row][col];
    }

    public static int minFallingPathSum(int[][] grid) {
        if(grid.length==1) return grid[0][0];
        MinimumFallingPathSumII m = new MinimumFallingPathSumII();
        int n= grid.length;
        System.out.println(n);
        int ans = Integer.MAX_VALUE;
        
        for(int i=0; i<n; i++) {
            ans = Math.min(ans, solve(grid,0,i,m));
        }
        return ans;
    }
    public static void main(String[] args) {
        // int[][] grid = {{1,2,3},{4,5,6},{7,8,9}};
        int[][] grid = {{7}};
        System.out.println("ans: "+minFallingPathSum(grid));
    }
}
