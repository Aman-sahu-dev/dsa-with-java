import java.util.Arrays;

public class RelativeRanks {
    public static String[] findRelativeRanks(int[] score) {
        String[] answer = new String[score.length];
        int[] temp = score.clone();
        String[] medal = {"Gold Medal","Silver Medal","Bronze Medal"};
        Arrays.sort(score);
        int n = score.length;

        for(int i=0;i<n;i++) {
            int s = score[n-i-1];
            for(int j=0;j<temp.length;j++) {
                if(temp[j]==s && i<3)
                    answer[j] = medal[i];
                    else if(temp[j]==s && i>=3){
                        answer[j] = String.valueOf(i+1);
                    }
            }
        }
        return answer;
    }
    public static void main(String[] args) {
        int[] score = {10,3,8,9,4};
        String[] answer = findRelativeRanks(score);
        for(String s : answer)
            System.out.print(s + ", ");
    }
}
