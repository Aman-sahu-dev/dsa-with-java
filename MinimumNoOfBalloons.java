import java.util.Arrays;

public class MinimumNoOfBalloons {

    public static int findMinArrowShots(int[][] points) {
        int arrow = 1;
        Arrays.sort(points, (a, b) -> Integer.compare(a[0], b[0]));

        int prevStart = points[0][0];
        int prevEnd = points[0][1];

        for (int i = 1; i < points.length; i++) {
            int currStart = points[i][0];
            int currEnd = points[i][1];

            if (currStart > prevEnd) {
                arrow++;
                prevStart = currStart;
                prevEnd = currEnd;
            } else {
                prevStart = Math.max(currStart, prevStart);
                prevEnd = Math.min(currEnd, prevEnd);
            }
        }
        return arrow;
    }

    public static void main(String[] args) {
        int points[][] = { { 10, 16 }, { 2, 8 }, { 1, 6 }, { 7, 12 } };
        System.out.println(findMinArrowShots(points));
    }
}
