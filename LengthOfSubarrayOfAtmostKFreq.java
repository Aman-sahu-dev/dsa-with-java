import java.util.HashMap;

public class LengthOfSubarrayOfAtmostKFreq {
    public static int maxSubarrayLength(int[] nums, int k) {
        HashMap<Integer, Integer> freq = new HashMap<>();
        int left = 0;
        int right = 0;
        int count = 0;

        while(right < nums.length){
            freq.put(nums[right], freq.getOrDefault(nums[right], 0)+1);
            while(left < right && freq.get(nums[right]) >k){
                freq.put(nums[right], freq.getOrDefault(nums[right],0) -1);
                left++;
            }
            count = Math.max(count , right - left +1);
            right++;
        }
        return count;
    }
    public static void main(String[] args) {
        // int nums[] = {1,2,3,1,2,3,1,2};
        int nums[] = {1,4,4,3};
        int k = 1;
        System.out.println(maxSubarrayLength(nums, k));
    }
}
