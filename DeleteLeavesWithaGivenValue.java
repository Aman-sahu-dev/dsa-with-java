public class DeleteLeavesWithaGivenValue {
    public static TreeNode removeLeafNodes(TreeNode root, int target) {
        System.out.print(root.val+" -> ");
        if(root.left == null && root.right == null && root.val == target){
            System.out.println("\n null : "+root.val);
            return root = null;
        };
        if(root.left!=null)
            root.left = removeLeafNodes(root.left, target);
        if(root.right!=null)
            root.right = removeLeafNodes(root.right, target);
        if(root.left == null && root.right == null && root.val == target)
            root = null;
        return root;
    }
    public static void main(String[] args) {
        TreeNode root = new TreeNode(1,new TreeNode(2,new TreeNode(2,null,null),null),new TreeNode(3,new TreeNode(2,null,null),new TreeNode(4)));

        TreeNode result = removeLeafNodes(root, 2);
        TreeNode.printTree(result);
    }
}
