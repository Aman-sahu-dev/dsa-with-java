import java.util.HashSet;

public class ReversePrefixofWord {
    public static String reversePrefix(String word, char ch) {
        String result ="";
        int index = word.indexOf(ch);
        String toreverse =  word.substring(0, index+1);
        for(int j=toreverse.length()-1;j>=0;j--) {
            result+=toreverse.charAt(j);
        }
        result += word.substring(index+1);
        return result;

    }
    public static void main(String[] args) {
        System.out.println(reversePrefix("abcdefg", 'd'));
    }
}
