public class LinkedListCycle {
    public static boolean hasCycle(ListNode head) {
        if(head == null)
            return false;
        else if(head.next== null)
            return false;
        else{
            ListNode fast = head;
            ListNode slow = head;
            
            while(fast != null && fast.next !=null) {
                fast = fast.next.next;
                slow = slow.next;
                System.out.println("fast: " + fast.val);
                System.out.println("slow: " + slow.val);
                if(fast == slow) return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(3);
        ListNode firstNode = new ListNode(2);
        ListNode secondNode = new ListNode(0);
        ListNode thirdNode = new ListNode(-4);

        head.next = firstNode;
        firstNode.next = secondNode;
        secondNode.next = thirdNode;
        thirdNode.next = firstNode;

        System.out.println(hasCycle(head));
    }

}
