import java.util.ArrayList;
import java.util.HashSet;

public class IntersectionOfTwoArray {
    public static int[] intersection(int[] nums1, int[] nums2) {
        HashSet<Integer> set1 = new HashSet<Integer>();
        ArrayList<Integer> arr = new ArrayList<>();
        for(int num: nums1)
            set1.add(num);

        for(int num: nums2){
            if(set1.contains(num)){
                if(!arr.contains(num))
                    arr.add(num);
            }
        }
        int[] result = new int[arr.size()];
        int i=0;
        for(int num: arr)
            result[i++] = num;
        return result;
    }

    public static void main(String[] args) {
        int[] nums1 = { 4, 9, 5 };
        int[] nums2 = { 9, 4, 9, 8, 4 };
        int result[] = intersection(nums1, nums2);
        for (int num : result)
            System.out.print(num + ", ");
    }
}
