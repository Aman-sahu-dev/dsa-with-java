import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class KthSmallestPrimeFraction {
    public static int[] kthSmallestPrimeFraction(int[] arr, int k) {
        int[] result = new int[2];
        HashMap<Float,Integer[]> map = new HashMap<>();
        for(int i = 0; i < arr.length; i++){
            for(int j = arr.length-1; j>i; j--){
                Integer[] v = {arr[i], arr[j]};
                map.put((float)arr[i]/arr[j], v); 
            }
        }
        ArrayList<Float> sortedKeys = new ArrayList<Float>(map.keySet());
        Collections.sort(sortedKeys);
        result[0] = map.get(sortedKeys.get(k-1))[0];
        result[1] = map.get(sortedKeys.get(k-1))[1];
        return result;
    }
    public static void main(String[] args) {
        int[] arr = {1,2,3,5};
        for(int i: kthSmallestPrimeFraction(arr, 3)){
            System.out.println(i);
        }
    }
}

