
public class SmallestStringStartingFromLeaf {
    static String str = "";
    static StringBuilder smallest;
    static StringBuilder current;

    public static String smallestFromLeaf(TreeNode root) {
        if(root!=null){
            str = ""+(char)(root.val+'a');
            dfs(root, str);
        }
        str = smallest.toString();
        smallest = null;
        current = null;
        return str;
    }
    static void dfs(TreeNode node, String str) {
        if(node.left==null && node.right==null){ 
            current = new StringBuilder(str).reverse();
            if(smallest==null) smallest = new StringBuilder(str).reverse();
            if(smallest.compareTo(current)>0) smallest = current;
            return;
        }

        if(node!=null){
            if(node.left!=null) dfs(node.left, str+(char)(node.left.val+'a'));
            if(node.right!=null) dfs(node.right, str+(char)(node.right.val+'a'));
        }
    }
    public static void main(String[] args) {

        TreeNode root = new TreeNode(0,new TreeNode(1,new TreeNode(3,null,null),new TreeNode(4,null,null)),new TreeNode(2,new TreeNode(3,null,null),new TreeNode(4,null,null)));

        TreeNode newNode = new TreeNode(3,new TreeNode(9,null,null),new TreeNode(20,new TreeNode(15,null,null),new TreeNode(7,null,null)));
        System.out.println(smallestFromLeaf(root));
        System.out.println(smallestFromLeaf(newNode));
    }
}
