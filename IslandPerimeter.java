public class IslandPerimeter {
    static int perimeter =0;
    public static int islandPerimeter(int[][] grid) {
        for(int i=0; i<grid.length; i++){
            for(int j=0; j<grid[i].length; j++){
                findperimeter(grid, i, j);
            }
        }
        return perimeter;
    }
    static void findperimeter(int[][] grid , int i , int j){
        if(i<0 || j<0 || i>=grid.length||j>=grid[i].length){
            return;
        }
        if(grid[i][j]==2){
            perimeter--;
            return;
        }
        else{
            if(grid[i][j]==1){
                grid[i][j] = 2;
                perimeter+=4;
                findperimeter(grid, i, j+1); //right
                findperimeter(grid, i, j-1); //left
                findperimeter(grid, i-1, j); //up
                findperimeter(grid, i+1, j); //down

            }
        }
    }
    public static void main(String[] args) {
        // int[][] grid = {{0,1,0,0}, {1,1,1,0}, {0,1,0,0}, {1,1,0,0}};
        int[][] grid = {{1,0}};
        System.out.println(islandPerimeter(grid));
    }
}
