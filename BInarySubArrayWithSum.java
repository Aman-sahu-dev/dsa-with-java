import java.util.HashMap;

public class BInarySubArrayWithSum {
    public static int numSubarraysWithSum(int[] nums, int goal) {
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        int sum = 0, count = 0;

        for (int num : nums) {
            sum += num;
            if (sum == goal)
                count++;
            if (map.containsKey(sum - goal))
                count += map.get(sum - goal);
            map.put(sum, map.getOrDefault(sum,0) +1);
        }
        return count;
    }

    public static void main(String[] args) {
        int nums[] = {1,0,1,0,1};
        int goal = 2;
        System.out.println(numSubarraysWithSum(nums, goal));
    }
}
