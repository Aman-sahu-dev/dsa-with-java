import java.util.ArrayList;

public class SumofDistancesinTree {
    static int[][] dp;
    static boolean[] vis;
    public static int[] sumOfDistancesInTree(int n, int[][] edges) {
        int[] answer = new int[n];
        ArrayList<ArrayList<Integer>> adj = new ArrayList<>();
        for(int i = 0; i < n; i++) 
            adj.add(new ArrayList<>());

        for(int[] egde : edges){
            int u = egde[0], v=egde[1];
            adj.get(u).add(v);
            adj.get(v).add(u);
        }
        System.out.println("Adjacency List: "+adj);

        dp = new int[n][n];
        vis = new boolean[n];
        SumofDistancesinTree s = new SumofDistancesinTree();

        for(int i=0; i<n; i++){
            answer[i] = findlength(i,s,adj,0);
            System.out.println("return:->"+ answer[i]);
            for(int j=0; j<n; j++) vis[j] = false;
        }
        return answer;
    }
    public static int findlength(int node, SumofDistancesinTree s,ArrayList<ArrayList<Integer>> adj,int sum) {
        if(s.vis[node]) return 0;
        s.vis[node] = true;
        
        ArrayList<Integer> neighbour = adj.get(node);
        System.out.println(node+" --> "+neighbour);
        if(neighbour.size()==1){
            System.out.println("nn");
            if(vis[neighbour.get(0)]) return 0;
        };

        for(int i=0;i<neighbour.size();i++){
            System.out.println("neighbour: -> "+ neighbour.get(i));
            if(s.vis[neighbour.get(i)]) continue;
            if(neighbour.get(i)!=node)
                sum = findlength(neighbour.get(i), s, adj, sum);
            System.out.println("sum "+sum);
        }

        System.out.println("curr node: " +node);
        if(neighbour.size()==1) sum+=0;
        return sum+=neighbour.size()+1;
    }
    public static void main(String[] args) {
        int[][] edges = {{0,1},{0,2},{2,3},{2,4},{2,5}};
        int[] answer = sumOfDistancesInTree(6, edges);
        for(int i: answer){
            System.out.print(i+", ");
        }
        
    }
}
