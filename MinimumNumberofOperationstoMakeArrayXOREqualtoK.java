public class MinimumNumberofOperationstoMakeArrayXOREqualtoK {
    public static int minOperations(int[] nums, int k) {
        int ans =0;
        for(int bit = 0; bit < 20; bit++) {
            int xorVal = 0;
            for(int el : nums){
                int bitval = (1<<bit) & el;
                xorVal = xorVal ^ bitval;
            }
            int Kbitval = (1<<bit)&k;
            if(xorVal != Kbitval) ans++;
        }
        return ans;
    }
    public static void main(String[] args) {
        int[] nums = {2,1,3,4};
        int k = 1;
        System.out.println("result: " + minOperations(nums, k));
    }
}
