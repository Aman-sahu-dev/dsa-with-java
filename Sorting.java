// Sorting algorithm:
// 1. Bubble Sorting -> O(n2) swapping each iteration.
// 2. Insertion Sort -> O(n2)
// 3. Quick Sort --> O(n log(n)) avg case. and O(n2) worst case.
// 4. Selection Sort -> O(n2) but better than bubble Sort. it have less swapping than bubble Sort.
// 5. Bucket Sort 
// 6. Radix Sort
// 7. Heap Sort
// 8. Merge Sort
// 9. Counting Sort

public class Sorting {

    // Print Array
    public static void printArray(int nums[]) {
        for (int num : nums)
            System.out.print(num + " ");
        System.out.println();
    }

    public static void bubbleSort(int nums[]) {
        int size = nums.length;
        int temp;

        System.out.print("Before sorting: ");
        printArray(nums);
        System.out.println();

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size - i - 1; j++) {
                if (nums[j] > nums[j + 1]) {
                    temp = nums[j];
                    nums[j] = nums[j + 1];
                    nums[j + 1] = temp;
                }
            }
            printArray(nums);
            System.out.println();
        }
    }

    public static void selectionSort(int[] nums) {
        int size = nums.length;

        System.out.print("Selection sort  \nBefore sorting: ");
        printArray(nums);

        for (int i = 0; i < size - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < size; j++) {
                if (nums[j] < nums[minIndex]) {
                    minIndex = j;
                }
            }
            int temp = nums[i];
            nums[i] = nums[minIndex];
            nums[minIndex] = temp;
        }
        System.out.println();
        printArray(nums);
    }

    public static void insertionSort(int nums[]) {
        int size = nums.length;
        System.out.print("Insertion sort  \nBefore sorting: ");
        printArray(nums);
        
        for (int i = 1; i < size; i++) {

            int j = i - 1;
            int key = nums[i];

            while (j >= 0 && nums[j] > key) {
                nums[j + 1] = nums[j];
                j--;
            }
            nums[j + 1] = key;
        }
        System.out.println();
        printArray(nums);
    }

    public static int partition(int[] nums, int low , int high) {
        int pivot = nums[high];
        int i = low-1;
        for(int j = low; j < high; j++){
            if(nums[j] <= pivot){
                i++;
                int temp = nums[j];
                nums[j] = nums[i];
                nums[i] = temp;
            }
        }
        int temp = nums[high];
        nums[high] = nums[i+1];
        nums[i+1] = temp;
        return i+1;
    }

    public static void quickSort(int[] nums, int low , int high){
        if(low<=high){
            int pivot = partition(nums, low, high);
            quickSort(nums, low,pivot-1);
            quickSort(nums, pivot+1, high);
        }
    }

    public static void merge(int[] nums, int left, int mid, int right){
        int n1 = mid-left+1;
        int n2 = right-mid;

        int lArr[] = new int[n1];
        int rArr[] = new int[n2];

        for(int x = 0; x < n1;x++){
            lArr[x] = nums[left+x];
        }
        for(int x = 0; x < n2;x++){
            rArr[x] = nums[mid+1+x];
        }
        int i=0;
        int j=0;
        int k=left;

        while(i<n1 && j<n2) {
            if(lArr[i]<rArr[j]){
                nums[k] = lArr[i];
                i++;
            }
            else{
                nums[k]= rArr[j];
                j++;
            }
            k++;
        }

        while(i<n1){
            nums[k] = lArr[i];
            i++;
            k++;
        }
        while(j<n2){
            nums[k] = rArr[j];
            j++;
            k++;
        }

    }
    
    public static void mergeSort(int[] nums, int left, int right) {
        if(left<right){
            int mid = (left + right)/2;
            mergeSort(nums, left, mid);
            mergeSort(nums, mid+1, right);

            merge(nums, left, mid , right);
        }
    }
    public static void main(String[] args) {

        int nums[] = { 6, 7, 2, 1, 9, 5, 2 };

        // bubbleSort(nums);
        // selectionSort(nums);
        // insertionSort(nums);
        // quickSort(nums, 0, nums.length-1);
        mergeSort(nums, 0, nums.length-1);
        printArray(nums);
    }
}
