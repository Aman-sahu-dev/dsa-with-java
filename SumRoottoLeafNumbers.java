public class SumRoottoLeafNumbers {
    int sum;
    public static int sumNumbers(TreeNode root) {
        SumRoottoLeafNumbers obj = new SumRoottoLeafNumbers();
        dfs(root,obj,0);
        return obj.sum;
    }

    static void dfs(TreeNode root, SumRoottoLeafNumbers obj,int num){
        if(root.left==null && root.right==null){
            num = num*10+root.val;
            obj.sum+=num;
        }
        if(root!=null){
            num = num*10+root.val;
            if(root.left!=null)
                dfs(root.left,obj,num);
            if(root.right!=null)
                dfs(root.right,obj,num);
        }
        else return;
    }
    public static void main(String[] args) {
        TreeNode root = new TreeNode(4,new TreeNode(9, new TreeNode(5,null,null),new TreeNode(1,null,null)),new TreeNode(0,null,null));

        System.out.println(sumNumbers(root));
    }
}
