import java.util.Arrays;

public class MaximizeHappinessofSelectedChildren {
    public static long maximumHappinessSum(int[] happiness, int k) {
        long happines = 0;
        int[] temp = happiness.clone();
        Arrays.sort(temp);
        int n = temp.length;
        for(int i=0; i<k; i++) {
            if(temp[n-i-1]-i>0){
                System.out.println(temp[n-i-1]-i);
                happines+=(temp[n-i-1] - i);
            }
        }
        return happines;
    }
    public static void main(String[] args) {
        int[] happiness = {2,5,4,3};
        System.out.println("result: "+ maximumHappinessSum(happiness, 1));
        int[] happiness3 = {1,1,1,1};
        System.out.println("result: "+ maximumHappinessSum(happiness3, 2));
        int[] happiness2 = {12,1,42};
        System.out.println("result: "+ maximumHappinessSum(happiness2, 3));
    }
}
