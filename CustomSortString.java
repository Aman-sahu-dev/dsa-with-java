import java.util.HashMap;
import java.util.Map;

public class CustomSortString {
    public static String customSortString(String order, String s) {
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        for(char ch : s.toCharArray())
            map.put(ch, map.getOrDefault(ch, 0)+1);

        StringBuilder result = new StringBuilder();

        for(char ch : order.toCharArray()){
            if(!map.containsKey(ch))
                continue;
            int count = map.get(ch);
            for(int i = 0; i < count; i++){
                result.append(ch);
                map.remove(ch);
            }
        }

        for(char ch : map.keySet()){
            int count = map.get(ch);
            for(int i = 0; i < count; i++){
            result.append(ch);
            }
        }
        return result.toString();
    }

    public static void main(String[] args) {
        System.out.println("Result: "+ customSortString("bcafg", "abcd"));
    }
}
