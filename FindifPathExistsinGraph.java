import java.util.ArrayList;

public class FindifPathExistsinGraph {
    static boolean[] vis;
    public static boolean validPath(int n, int[][] edges, int source, int destination) {
        ArrayList<ArrayList<Integer>> arr = new ArrayList<ArrayList<Integer>>();
        vis = new boolean[n];
        for(int i = 0; i <n; i++)
            arr.add(new ArrayList<>());
        
        for(int i = 0; i < edges.length; i++){
            int u = edges[i][0], v = edges[i][1];
            arr.get(u).add(v);
            arr.get(v).add(u);
        }
        System.out.println(arr);
        dfs(source ,arr,vis);
        return vis[destination];
    }
    static void dfs(int node,ArrayList<ArrayList<Integer>> arr,boolean[] vis){
        System.out.println("node: "+ node);
        if(vis[node]) return;
        vis[node] = true;
        System.out.println(node +"--> "+ arr.get(node));
        for(int i = 0; i <arr.get(node).size(); i++){  
            // System.out.println(arr.get(node).get(i));
            System.out.println("i: "+ i+ ", node[i]: "+ arr.get(node).get(i));  
            if(!vis[arr.get(node).get(i)])
                dfs(arr.get(node).get(i), arr, vis);
        }
    }
    public static void main(String[] args) {
        int n = 10;
        // int[][] edges = {{0,1},{0,2},{3,5},{5,4},{4,3}};
        // int[][] edges = {{0,7},{0,8},{6,1},{2,0},{0,4},{5,8},{4,7},{1,3},{3,5},{6,5}};
        int[][] edges = {{4,3},{1,4},{4,8},{1,7},{6,4},{4,2},{7,4},{4,0},{0,9},{5,4}};
        int source = 5, destination = 9;

        System.out.println(validPath(n,edges, source, destination));
    }
}
