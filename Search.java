// Searching algorithms --> linear search O(n), binary search O(log(n))
public class Search{

    public static void binarySearch(int nums[] , int target){
        int left = 0;
        int right = nums.length-1;

        while (left<=right) {
            int mid = (left+right)/2;

            if(nums[mid]==target){
                System.out.println("found at index: " + mid);
                return;
            }
            else if(target<nums[mid])
                right = mid-1;
            else if(target>nums[mid])
                left = mid+1;
        }
        System.out.println("Not found in array");
    }

    public static void linearSearch(int nums[] , int target){
        for(int i=0; i<nums.length; i++){
            if(nums[i]==target){
                System.out.println("Found at index: " +  i);
                return;
            }
        }
        System.out.println("Not found by linear search " );
    }
    public static void main(String[] args) {
        int nums[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17};
        int target = 18;
        binarySearch(nums, target);
        linearSearch(nums, target);
    }
}