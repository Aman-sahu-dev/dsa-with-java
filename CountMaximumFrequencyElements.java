import java.util.HashMap;

public class CountMaximumFrequencyElements {
    public  static int maxFrequencyElements(int[] nums) {
        int maxFreq = 0;
        HashMap<Integer, Integer> frequencyMap = new HashMap<>();
        for(int num : nums) {
            if(!frequencyMap.containsKey(num)){
                frequencyMap.put(num, 1);
            }else{
                frequencyMap.put(num,frequencyMap.get(num)+1);
            }
            maxFreq = Math.max(maxFreq, frequencyMap.get(num));
        }
        int count =0;
        for(int num : nums) {
            if(frequencyMap.get(num)==maxFreq){
                count++;
            }
        }
        return count;
    }
    public static void main(String[] args) {
        int nums[] = {1,2,2,3,1,4};
        System.out.println(maxFrequencyElements(nums));
    }
}
