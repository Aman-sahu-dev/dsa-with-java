import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class MinimumHeightTrees {
    public static List<Integer> findMinHeightTrees(int n, int[][] edges) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        
        if(n<=0) return result;

        if(n==1){
            result.add(0);
            return result;
        }

        ArrayList<ArrayList<Integer>> adj = new ArrayList<ArrayList<Integer>>();
        int[] deg = new int[n];

        for(int i = 0; i < n; i++)
            adj.add(new ArrayList<>());
        for(int i = 0; i < edges.length; i++){
            int u = edges[i][0], v = edges[i][1];
            deg[u]++;
            deg[v]++; 
            adj.get(u).add(v);
            adj.get(v).add(u);
        }
        Queue<Integer> q = new LinkedList<Integer>();
        for(int i=0;i<deg.length;i++){
            if(deg[i]==1) q.add(i);
        }

        while(n>2){
            int sz = q.size();
            n-=sz;
            while(sz-->0){
                int ele = q.poll();
                for(int el : adj.get(ele)){
                    deg[el]--;
                    if(deg[el]==1) q.add(el);
                }
            }
        }

        while(!q.isEmpty()){
            int ele = q.poll();
            result.add(ele);
        }
        
        return result;
    }
    
    public static void main(String[] args) {
        int n = 4;
        int[][] edges = {{1,0},{1,2},{1,3}};   
        System.out.println(findMinHeightTrees(n, edges));
    }
}