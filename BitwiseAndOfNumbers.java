public class BitwiseAndOfNumbers {
    public static int rangeBitwise(int a, int b) {
        if(a==b)
            return 0;
        int res = a;
        for(int i=a+1; i<=b; i++) {
            res = res&i;
            if(res==0 || i==b ||res==a){
                return res;
            }   
            System.out.println("Result :"+ res);
        }
        return res;
    }
    public static void main(String[] args) {
        // System.out.println("Bitwise of number are :"+ rangeBitwise(1073741824, 2147483647));
        System.out.println("Bitwise of number are :"+ rangeBitwise(2147483646, 2147483647));
        // System.out.println("Bitwise of number are :"+ rangeBitwise(5,7 ));
        // System.out.println("Bitwise of number are :"+ rangeBitwise(1,2 ));

        System.out.println("Finished");
    }
}
