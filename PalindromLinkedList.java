import java.util.ArrayList;

public class PalindromLinkedList {
    public static boolean isPalindrome(ListNode head) {
        if(head == null) return true;
        ArrayList<Integer> list = new ArrayList<>();
        ListNode node = head.next;

        while(node != null) {
            list.add(node.val);
            node = node.next;
        }
        int i =0; 
        int j = list.size()-1;
        System.out.println(list);
        while(i<j){
            if(list.get(i++)!=list.get(j--))
                return false;
        }
        return true;
    }
    public static void main(String[] args) {
        ListNode head = new ListNode();
        ListNode firstNode = new ListNode(1);
        head.next = firstNode;
        ListNode secondNode = new ListNode(2);
        firstNode.next = secondNode;
        ListNode thirdNode = new ListNode(1);
        secondNode.next = thirdNode;
        ListNode fourth = new ListNode(1);
        thirdNode.next = fourth;

        System.out.println(isPalindrome(head));
    }
}
