public class SumofLeftLeaves {
    private int sum;

    public static int sumOfLeftLeaves(TreeNode root) {
        SumofLeftLeaves obj = new SumofLeftLeaves();
        if(root!= null)
        calSum(root, obj, false);
        return obj.sum;
    }
    public static void calSum(TreeNode root,SumofLeftLeaves obj,boolean bool){
        
        if(root.left == null &&root.right ==null  && bool == true){
            obj.sum+=root.val;
            return;
        }
        if(root.left == null &&root.right ==null  && bool == false) return;

        if(root !=null){
            if(root.left!=null)
            calSum(root.left, obj,true);
            if(root.right!=null)
            calSum(root.right, obj,false);
        }
        else return;
        return;
    }


    public static void main(String[] args) {
        TreeNode root = new TreeNode(3, new TreeNode(9,null ,null), new TreeNode(20, new TreeNode(15,null,null), new TreeNode(7,null,null)));

        System.out.println(sumOfLeftLeaves(root));
    }
}

