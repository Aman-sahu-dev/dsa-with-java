import java.util.LinkedList;
import java.util.Queue;

public class EvenOddTree {
    public boolean isEvenOddTree(TreeNode root) {
        if(root == null) return true;

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        boolean evenlevel = true;

        while(queue.size() > 0) {
            int size = queue.size();
            int prev = evenlevel ? Integer.MIN_VALUE: Integer.MAX_VALUE;

            while(size-- >0){
                root = queue.poll();
                if(evenlevel && (root.val%2 == 0 || root.val <= prev)) return false;
                if(!evenlevel && (root.val%2 == 1 || root.val >= prev)) return false;
                prev = root.val;
                if(root.left != null)
                    queue.add(root.left);
                if(root.right != null)
                    queue.add(root.right);
                }
            evenlevel = !evenlevel;
        }

        return evenlevel;
    }

    public static void main(String[] args) {

    }
}
