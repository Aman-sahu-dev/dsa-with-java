public class LinkedList {

    private  static Node head;
    private int length;

    class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }
    }

    public LinkedList(int value) {
        Node newNode = new Node(value);
        head = newNode;
        length = 1;
    }

    public Node getHead() {
        return head;
    }

    public int getLength() {
        return length;
    }

    public void printList() {
        Node temp = head;
        while (temp != null) {
            System.out.println(temp.value);
            temp = temp.next;
        }
    }

    public void printAll() {
        if (length == 0) {
            System.out.println("Head: null");
        } else {
            System.out.println("Head: " + head.value);
        }
        System.out.println("Length:" + length);
        System.out.println("\nLinked List:");
        if (length == 0) {
            System.out.println("empty");
        } else {
            printList();
        }
    }
    
    public void makeEmpty() {
        head = null;
        length = 0;
    }
    
    public void append(int value) {
        Node newNode = new Node(value);
        if (length == 0) {
            head = newNode;
        } else {
            Node current = head;
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        }
        length++;
    }
    public  static void reverseBetween(int m , int n){
        Node dummy = new Node(0);
        dummy.next = head;
        Node preLeft = dummy;
        Node current = head;

        for(int i = 0; i < m-1; i++){
            preLeft = preLeft.next;
            current = current.next;
        }
        Node sublist = current;
        Node prev = null;
        for(int i=0;i<=n-m;i++){
            Node nextNode = current.next;
            current.next = prev;
            prev = current;
            current = nextNode;
        }
        preLeft.next = prev;
        sublist.next = current;
    }
    public static void main(String[] args) {
        LinkedList list = new LinkedList(0);
        list.append(1);
        list.append(2);
        list.append(3);
        list.append(4);
        list.append(5);
        System.out.println("List");
        list.printList();
        reverseBetween(3,5);
    }
}