public class palindromeNumber {
    public static boolean palindrome(int x){
        if(x<0)
            return false;
        int res = 0;
        int num = x;
        while(num!=0){
            res = res*10 + num%10;
            num = num/10;
        }
        if(res == x)
            return true;
        return false;
    }
    public static void main(String[] args) {
       System.out.println(palindrome(1201));
    }
}
