public class FirstMissingPositive {
    public static int firstMissingPositive(int[] nums) {
        int n = nums.length;
        int i=0;

        while(i<n){
            int corretIndex = nums[i]-1;
            
            if(nums[i]>0 && nums[i]<=n && nums[i]!= nums[corretIndex]){
                swap(nums,i,corretIndex);
            }
            else{
                i++;
            }
        }

        for(int j=0;j<n;j++){
            if(nums[j] != j+1){
                return j+1;
            }
        }
        return n+1;
    }
    public static void swap(int[] nums, int i, int j){
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
    public static void main(String[] args) {
        int[] nums = {7,8,9,11,12};
        System.out.println(firstMissingPositive(nums));
    }
}
