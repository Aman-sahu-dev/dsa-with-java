public class patterns {
    public static void printSquare(int n){
        System.out.println("1. Square \n");
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                System.out.print("* ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void rightTriangle(int n){
        System.out.println("2. Right triangle \n");
        for(int i = 0; i < n; i++){
            for(int j = 0; j <=i; j++){
                System.out.print("* ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void rightNumberTriangle(int n){
        System.out.println("3. Right Number Triangle \n");
        for(int i = 1; i <=n; i++){
            for(int j = 1; j <=i; j++){
                System.out.print(j+" ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void rightRowNumberTriangle(int n){
        System.out.println("4. Right Row Number Triangle \n");
        for(int i = 1; i <=n; i++){
            for(int j = 1; j <=i; j++){
                System.out.print(i+" ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void reverseTriangle(int n){
        System.out.println("5. Reverse Triangle \n");
        for(int i = n; i >=1; i--){
            for(int j = 1; j <=i; j++){
                System.out.print("* ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void reverseNumberTriangle(int n){
        System.out.println("6. Reverse Number Triangle \n");
        for(int i = n; i >=1; i--){
            for(int j = 1; j <=i; j++){
                System.out.print(j+" ");
            }
            System.out.println();
        }
        System.out.println();
    }
    
    public static void Pyramid(int n){
        System.out.println("7. Pyramid \n");
        for(int i=1; i<=n; i++){
            for(int j=n-i; j>0; j--){
                System.out.print(" ");
            }
            for(int k=1; k<=(2*i-1); k++){
                System.out.print("*");
            }
            for(int j=n-i; j>0; j--){
                System.out.print(" ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void reversePyramid(int n){
        System.out.println("8. Reverse Pyramid \n");
        for(int i=n; i>=1; i--){
            for(int j=n-i; j>0; j--){
                System.out.print(" ");
            }
            for(int k=1; k<=(2*i-1); k++){
                System.out.print("*");
            }
            for(int j=n-i; j>0; j--){
                System.out.print(" ");
            }
            System.out.println();
        }
        System.out.println();
    }
    
    public static void diamond(int n){
        System.out.println("9. Diamond \n");
        for(int i=1; i<=n; i++){
            for(int j=n-i; j>0; j--){
                System.out.print(" ");
            }
            for(int k=1; k<=(2*i-1); k++){
                System.out.print("*");
            }
            for(int j=n-i; j>0; j--){
                System.out.print(" ");
            }
            System.out.println();
        }
        for(int i=n; i>=1; i--){
            for(int j=n-i; j>0; j--){
                System.out.print(" ");
            }
            for(int k=1; k<=(2*i-1); k++){
                System.out.print("*");
            }
            for(int j=n-i; j>0; j--){
                System.out.print(" ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void rightHalfPyramid(int n){
        System.out.println("10. Right Half Pyramid \n");
        for(int i=1; i<=n; i++){
            for(int k=1; k<=i; k++){
                System.out.print("*");
            }
            for(int j=n-i; j>0; j--){
                System.out.print(" ");
            }
            System.out.println();
        }
        for(int i=n-1; i>=1; i--){
            for(int k=1; k<=i; k++){
                System.out.print("*");
            }
            for(int j=n-i; j>0; j--){
                System.out.print(" ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void alternateOneandZero(int n){
        System.out.println("11. Zero one Right Triangle \n");
        int start = 1;
        for(int i=0; i<n; i++){
            if(i%2==0) start = 1;
            else start = 0;

            for(int j=0; j<=i; j++){
                System.out.print(start+ " ");
                start = 1 - start;
            }
            System.out.println();
        }
        System.out.println();
    }
    
    public static void numberCrown(int n){
        System.out.println("12. Number Crown \n");
        int space = 2*(n-1);
        for(int i = 1; i <=n; i++){
            for(int j = 1; j <=i; j++){
                System.out.print(j+" ");
            }
            for(int j = 0; j<space; j++){
                System.out.print("  ");
            }
            for(int j = i; j>=1; j--){
                System.out.print(j+" ");
            }
            System.out.println();
            space -=2;
        }
        System.out.println();
    }
    
    public static void nNumberTriangle(int n){
        System.out.println("13. N Number Triangle \n");
        int counter = 1;
        for(int i = 1; i <=n; i++){
            for(int j = 1; j <=i; j++){
                System.out.print(counter++ + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void nLetterTriangle(int n){
        System.out.println("14. N Letter Triangle \n");
        for(int i = 0; i <n; i++){
            for(char j = 'A'; j <='A'+i; j++){
                System.out.print(j + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void reverseLetterTriangle(int n){
        System.out.println("15. Reverse Letter Triangle \n");
        for(int i = n; i >=1; i--){
            for(char j = 'A'; j <'A'+i; j++){
                System.out.print(j + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void alphaRamp(int n){
        System.out.println("16. Alpha Ramp \n");
        for(int i = 0; i <n; i++){
            for(int j = 0; j <=i; j++){
                System.out.print((char)(i+'A') + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void alphaHill(int n){
        System.out.println("17. Alpha Hill \n");
        for(int i=1; i<=n; i++){
            char ch = 'A';
            int breakPoint = ((2*i - 1)/2) + 1;
            for(int j=n-i; j>0; j--){
                System.out.print("  ");
            }
            for(int k=1; k<=(2*i-1); k++){
                System.out.print(ch+ " ");
                if(breakPoint > k) ch++;
                else ch--;
            }
            for(int j=n-i; j>0; j--){
                System.out.print("  ");
            }
            System.out.println();
        }
        System.out.println();
    }
    
    public static void alphaTriangle(int n){
        System.out.println("18. Alpha triangle \n");
        for(int i = 0; i < n; i++){
            for(char j = (char)('A'+ n-1); j < 'A'+n +i; j++){
                System.out.print((char)(j-i) + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
    public static void symmetry(int n){
        System.out.println("19. Hollow diamond \n");
        for(int i = 0; i < n; i++){
            for(int j = n-i; j >= 1; j--){
                System.out.print("*");
            }
            for(int j=0; j<2*i; j++){
                System.out.print(" ");
            }
            for(int j = n-i; j >= 1; j--){
                System.out.print("*");
            }
            System.out.println();
        }
        for(int i = n-1; i >=0; i--){
            for(int j = n-i; j >= 1; j--){
                System.out.print("*");
            }
            for(int j=0; j<2*i; j++){
                System.out.print(" ");
            }
            for(int j = n-i; j >= 1; j--){
                System.out.print("*");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void butterfly(int n){
        System.out.println("20. Butterfly \n");
        for(int i = 1; i <=n; i++){
            for(int j = 1; j <=i; j++){
                System.out.print("*");
            }
            for(int j=0; j<2*n-2*i; j++){
                System.out.print(" ");
            }
            for(int j = 1; j <=i; j++){
                System.out.print("*");
            }
            System.out.println();
        }
        for(int i = n-1; i >=1; i--){
            for(int j = 1; j <=i; j++){
                System.out.print("*");
            }
            for(int j=0; j<2*n-2*i; j++){
                System.out.print(" ");
            }
            for(int j = 1; j <=i; j++){
                System.out.print("*");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void hollowSqaure(int n){
        System.out.println("21. Hollow Sqaure");
        for(int i = 1; i <=n; i++){
            for(int j = 1; j <=n; j++){
                if(i==1 || j==1 || j==n|| i==n)
                    System.out.print("* ");
                else
                    System.out.print("  ");
            }
            System.out.println();
        }
    }
    public static void spiralPattern(int n){
        System.out.println("22. Spiral pattern");
        for(int i = 0; i <2*n-1; i++){
            for(int j = 0; j <2*n-1; j++){
                int top = i;
                int left = j;
                int right = (2*n-2)-j;
                int bottom = (2*n-2)-i;
                System.out.print(n- Math.min(Math.min(top, left),Math.min(right, bottom))+ " ");
            }
            System.out.println();
        }
    }
    public static void main(String[] args) {
        int n = 5;
        printSquare(n);
        rightTriangle(n);
        rightNumberTriangle(n);
        rightRowNumberTriangle(n);
        reverseTriangle(n);
        reverseNumberTriangle(n);
        Pyramid(n);
        reversePyramid(n);
        diamond(n);
        rightHalfPyramid(n);
        alternateOneandZero(n);
        numberCrown(n);
        nNumberTriangle(n);
        nLetterTriangle(n);
        reverseLetterTriangle(n);
        alphaRamp(n);
        alphaHill(n);
        alphaTriangle(n);
        symmetry(n);
        butterfly(n);
        hollowSqaure(n);
        spiralPattern(n);
    }
}
