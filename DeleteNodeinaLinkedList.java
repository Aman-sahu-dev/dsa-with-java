public class DeleteNodeinaLinkedList {
    public static void deleteNode(ListNode node) {
        node.val = node.next.val;
        node.next = node.next.next;
    }
    public static void main(String[] args) {
        ListNode a = new ListNode(4);
        ListNode head = a;
        ListNode b = new ListNode(5);
        a.next = b;
        ListNode c = new ListNode(1);
        b.next = c;
        ListNode d = new ListNode(9);
        c.next = d;
        System.out.println("Before deleting: ");
        ListNode.printList(head);
        deleteNode(c);
        ListNode.printList(head);
    }
    
}
