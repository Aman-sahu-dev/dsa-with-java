public class RemoveDuplicateFromSortedArray {
    public static int removedDuplicate(int[] array){
        int count = 0;
        for(int i=0; i<array.length; i++){
            if(i<array.length-1 && array[i]== array[i+1]){
                continue;
            }
            else{
                array[count] = array[i];
                count++;
            }
        }
        return count;
    }
    public static void main(String[] args) {
        int[] array = {1,1,2,2,3,3,4,5,6,7,8};
        int result = removedDuplicate(array);
        System.out.print(result+ " ");
    }
}
