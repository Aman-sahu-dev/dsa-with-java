import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class OpentheLock {
    public static int openLock(String[] deadends, String target) {
        int level = 0;
        String start = "0000";
        Queue<String> queue = new LinkedList<>();
        HashSet<String> set = new HashSet<>();

        queue.add(start);
        for(String deadend : deadends){
            set.add(deadend);
        }
        if(set.contains(start)) return -1;
        set.add(start);

        while(!queue.isEmpty()){
            int size = queue.size();
            while(size-->0) {
                String element = queue.poll();

                if(element.equals(target)) return level;
                if(!set.contains(element)) continue;

                for(int c=0; c<4; c++){
                    StringBuilder str = new StringBuilder(element);
                    char ch = element.charAt(c);
                    char left = (char)(ch=='0'?'9': ch-1);
                    str.setCharAt(c, left);

                    if(!set.contains(str.toString())){
                        queue.add(str.toString());
                        set.add(str.toString());
                    }

                    char right = (char)(ch=='9'?'0':ch+1);
                    str.setCharAt(c, right);
                    if(!set.contains(str.toString())){
                        queue.add(str.toString());
                        set.add(str.toString());
                    }
                }
            }
            level++;
        }
        
        return -1;
    }
    public static void main(String[] args) {
        String[] deadends = {"0201","0101","0102","1212","2002"};
        String target = "0202";
        // String target = "1100";
        System.out.println(openLock(deadends, target));
    }
}
