public class MinimumCommonValue {
    public static int getCommon(int[] nums1, int[] nums2) {
        int i=0,j=0;
        int m = nums1.length;
        int n = nums2.length;

        while(i<m && j<n) {
            if(nums1[i] == nums2[j]){
                return nums1[i];
            }
            else{
                if(nums1[i]<nums2[j]){
                    i++;
                }
                else{
                    j++;
                }
            }
        }
        return -1;
    }
    public static void main(String[] args) {
        int nums1[] = {1,2,3,6};
        int nums2[] = {2,3,4,5};
        System.out.println(getCommon(nums1, nums2));
    }
}
