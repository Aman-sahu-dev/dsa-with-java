public class PowerOfTwo {
    public static boolean powerOfTwo(int n){
        if(n <=0) return false;
        while(n!=1){
            if(n%2==1)
            return false;
            n = n/2;
        }
        return true;

    }
    public static void main(String[] args) {
        System.out.println("The number is power of two: " + powerOfTwo(3));
    }
}
