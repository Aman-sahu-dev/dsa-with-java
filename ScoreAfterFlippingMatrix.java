public class ScoreAfterFlippingMatrix {
    public static int matrixScore(int[][] grid) {
        int n = grid.length, m = grid[0].length;
        int ans = n*(1<<(m-1-0));

        for(int j=1; j<m; j++) {
            int count = 0;
            for(int i=0; i<n; i++) {
                count += (grid[i][0]==0) ? (grid[i][j]^1) : grid[i][j];
            }
            if(count < (n-count))
                ans+= (n-count)*(1<<(m-1-j));
            else
                ans+= count*(1<<(m-1-j));
        }
        return ans;
    }

    public static void main(String[] args) {
        int[][] grid = { { 0, 0, 1, 1 }, { 1, 0, 1, 0 }, { 1, 1, 0, 0 } };
        System.out.println("result: " + matrixScore(grid));
    }
}