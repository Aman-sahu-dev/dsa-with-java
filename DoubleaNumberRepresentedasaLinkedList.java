
public class DoubleaNumberRepresentedasaLinkedList {
    public static ListNode reverseList(ListNode head) {
        if(head.next == null) return head;
        ListNode prev = null;
        ListNode curr = head;
        ListNode currNext = curr;
        if (curr.next != null)
            currNext = curr.next;
        while (currNext != null) {
            curr.next = prev;
            prev = curr;
            curr = currNext;
            currNext = currNext.next;
        }
        curr.next = prev;
        head = curr;
        return head;
    }

    public static ListNode doubleIt(ListNode head) {
        ListNode newHead = reverseList(head);
        ListNode temp = newHead;
        int carry =0;
        while(temp != null){
            int doubleval = temp.val*2;
            System.out.println("double val: "+ doubleval);
            if(doubleval > 9){
                temp.val = (doubleval%10)+carry;
                carry = doubleval/10;
            }else{
                System.out.println("else: ");
                temp.val = temp.val*2+ carry;
                System.out.println("temp:"+ temp.val);
                if(carry>0) carry--;
            }
            temp = temp.next;
        }
        newHead = reverseList(newHead);
        if(carry>0){
            ListNode node = new ListNode(carry);
            node.next = newHead;
            newHead = node;
        }
        return newHead;
    }

    public static void main(String[] args) {
        ListNode a = new ListNode(1);
        ListNode head = a;
        ListNode b = new ListNode(0);
        a.next = b;
        // ListNode c = new ListNode(9);
        // b.next = c;
        System.out.println("Before :");
        ListNode.printList(head);
        System.out.println("\nAfter :");
        ListNode.printList(doubleIt(head));
    }
}
