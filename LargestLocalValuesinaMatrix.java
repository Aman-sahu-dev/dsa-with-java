public class LargestLocalValuesinaMatrix {
    public static int[][] largestLocal(int[][] grid) {
        int n = grid.length;
        int[][] result = new int[n-2][n-2];
        int m = result.length;
        for(int i = 0; i <m; i++) {
            for(int j = 0; j <m; j++) {
                result[i][j] = findmax(i,j,grid,n);
            }
        }
        return result;
    }
    public static int findmax(int a, int b,int[][] grid,int n) {
        int max = 0;
        for(int i=a;i<a+3;i++){
            for(int j=b;j<b+3;j++){
                if(grid[i][j]>max)
                    max = grid[i][j];
            }
        }
        return max;
    }
    public static void main(String[] args) {
        int grid[][] = {{1,1,1,1,1},{1,1,1,1,1},{1,1,2,1,1},{1,1,1,1,1},{1,1,1,1,1}};
        int[][] result = largestLocal(grid);
        for(int[] i : result){
            for(int j : i)
                System.out.print(j+", ");
            System.out.println();
        }
    }
}
