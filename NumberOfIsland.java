public class NumberOfIsland {
    public static int numIslands(char[][] grid) {
        int row = grid.length;
        if(row == 0) return 0;
        int col = grid[0].length;

        int numIslands = 0;

        for(int i = 0; i < row; i++) {
            for(int j = 0; j < col; j++) {
                if(grid[i][j]=='1'){
                    markIsland(grid,i,j,row,col);
                    numIslands++;
                }
            }
        }   
        return numIslands;
    }


    static void markIsland(char[][] grid,int i,int j, int row, int col) {
        if(i<0 || i>=row || j<0 || j>=col || grid[i][j]!='1') return;

        //marking visited
        grid[i][j] = '2';

        markIsland(grid, i+1, j, row, col); //down
        markIsland(grid, i, j+1, row, col); //right
        markIsland(grid, i-1, j, row, col); //up
        markIsland(grid, i, j-1, row, col); //left

    }
    public static void main(String[] args) {
        char[][] grid = {{'1','1','1','1','0'}, {'1','1','0','1','0'}, {'1','1','0','0','0'}, {'0','0','0','0','0'}};
        
        char[][] grid2 = {{'1','1','0','0','0'}, {'1','1','0','0','0'}, {'0','0','1','0','0'}, {'0','0','0','1','1'}};
        System.out.println(numIslands(grid2));
 
    }
}
