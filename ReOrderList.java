import java.util.Stack;

public class ReOrderList {
    public static void reorderList(ListNode head) {
        Stack<ListNode> s = new Stack<>();
        ListNode curr = head;
        while(curr != null) {
            s.push(curr);
            curr = curr.next;
        }
        curr = head;
        ListNode next = head.next;
        int n = s.size();
        int i=0;
        while(i<n/2) {
            curr.next = s.pop();
            curr.next.next = next;
            curr = next;
            next = next.next;
            i++;
        }
        curr.next = null;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode();
        ListNode one = new ListNode(1);
        head.next = one;
        ListNode two = new ListNode(2);
        one.next = two;
        ListNode three = new ListNode(3);
        two.next = three;
        ListNode four = new ListNode(4);
        three.next = four;
        ListNode five = new ListNode(5);
        four.next = five;

        reorderList(head.next);
    }
}
