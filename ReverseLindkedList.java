public class ReverseLindkedList {
    public  static ListNode reverseList(ListNode head) {
        if(head == null)
            return head;
        ListNode prev = null;
        ListNode current = head;
        ListNode next = current.next;

        while(current != null) {
            current.next = prev;
            prev = current;
            current = next;
            if(next != null)
                next = current.next;
        }
        head = prev;
        return head;
    }
    public static void main(String[] args) {
        ListNode head = new ListNode();
        ListNode first = new ListNode(1);
        head.next = first;
        ListNode second = new ListNode(2);
        first.next = second;
        ListNode third = new ListNode(3);
        second.next = third;
        ListNode fourth = new ListNode(4);
        third.next = fourth;
        ListNode fifth = new ListNode(5);
        fourth.next = fifth;

        ListNode node = reverseList(head);
        while(node != null) {
            System.out.print(node.val+"--> ");
            node = node.next;
        }
    }
}
