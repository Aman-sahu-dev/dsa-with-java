
public class LengthOfTheBinaryTree {
    public static int diameterOfBinaryTree(TreeNode root) {
        int[] diameter = new int[1];
        height(root, diameter);
        return diameter[0];
    }

    public static int height(TreeNode node, int[] diameter) {
        if(node == null)
            return 0;
        int lh = height(node.left, diameter);
        int rh = height(node.right, diameter);
        diameter[0] = Math.max(diameter[0],lh + rh);
        return 1+ Math.max(lh, rh);
    }
    public static void main(String[] args) {
        TreeNode node = new TreeNode(1,new TreeNode(2,new TreeNode(4),new TreeNode(5)),new TreeNode(3));
        System.out.println("Result: " + diameterOfBinaryTree(node));
    }
}
