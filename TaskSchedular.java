import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TaskSchedular {
    public static int getMinMachines(List<Integer> start, List<Integer> end) {
        // Combine start and end times and sort them
        List<int[]> events = new ArrayList<>();
        for (int i = 0; i < start.size(); i++) {
            events.add(new int[] { start.get(i), 1 }); // 1 for start
            events.add(new int[] { end.get(i), -1 }); // -1 for end
        }
        Collections.sort(events, Comparator.comparingInt(a -> a[0]));

        int machines = 0;
        int availableMachines = 0;

        for (int[] event : events) {
            // If a task starts, we need a new machine if no machine is available
            if (event[1] == 1) {
                if (availableMachines <= 0) {
                    machines++;
                    System.out.println("machines: " + machines);
                } else {
                    availableMachines--;
                    System.out.println("available: " + availableMachines);
                }
            }
            // If a task ends, free up a machine
            else {
                availableMachines++;
                System.out.println("available: ..." + availableMachines);
            }
            availableMachines = Math.max(0, availableMachines);
        }

        return machines;
    }

    public static void main(String[] args) {
        List<Integer> start = List.of(1, 8, 3, 9, 6);
        List<Integer> end = List.of(7, 9, 6, 14, 7);
        System.out.println(getMinMachines(start, end)); // Output: 3
    }
}
