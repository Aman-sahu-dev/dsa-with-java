import java.util.Stack;

public class MiniRemovetoMakeValid{
    public static String minRemoveToMakeValid(String s) {
        StringBuilder sb = new StringBuilder(s);
        Stack<Integer> stack = new Stack<Integer>();

        for(int i=0; i<s.length(); i++) {
            if(s.charAt(i) == '('){
                stack.push(i);
            }
            else if(s.charAt(i) == ')') {
                if(!stack.isEmpty()){
                    stack.pop();
                }
                else{
                    sb.setCharAt(i, '*');
                }
            }
        }
        while(!stack.isEmpty()){
            sb.setCharAt(stack.pop(), '*');
        }
        return sb.toString().replaceAll("\\*", "");
    }
    public static void main(String[] args) {
        System.out.println(minRemoveToMakeValid("lee(t(c)o)de)"));
    }
}