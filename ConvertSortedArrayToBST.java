
public class ConvertSortedArrayToBST {
    public static TreeNode sortedArrayToBST(int[] nums) {
        TreeNode node = new TreeNode();
        int n = nums.length-1;
        int i = n/2;
        int j = (n/2)+1;
        while(i>=0 && j<=n) {
            System.out.println("i : "+ i+ " j : "+ j);
            node.val = nums[i];
            node.left = new TreeNode(nums[--i]);
            node.right = new TreeNode(nums[j++]);
            System.out.println("value : "+ node.val+ " left : "+ node.left.val + " right : "+ node.right.val);
        }
        return node;
    }

    public static void main(String[] args) {
        int nums[] = { -10, -3, 0, 5, 9 };
        System.out.println(sortedArrayToBST(nums).val);
    }
}
