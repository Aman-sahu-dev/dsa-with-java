public class ReverseNumber {
    public static int reverse(int x) {
        int num = Math.abs(x);
        int reverse = 0;
        while(num!=0){
            if (reverse > (Integer.MAX_VALUE - num%10) / 10) {
                return 0;
            }
            reverse = reverse*10 + num%10;
            num/=10;
        }
        return (x<0) ? (-reverse): reverse;
    }

    public static void main(String[] args) {
        System.out.println(reverse(1534236469));
    }
}
