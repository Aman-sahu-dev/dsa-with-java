import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class RevealCardsInIncreasingOrder {

    public static int[] deckRevealedIncreasing(int[] deck) {
        int ordered[] = new int[deck.length];
        Queue<Integer> queue = new LinkedList<>();
        Arrays.sort(deck);
        for(int i = deck.length - 1; i >= 0; i--){
            if(!queue.isEmpty()){
                queue.add(queue.poll());
                queue.add(deck[i]);
            }else{
                queue.add(deck[i]);
            }
        }
        int i =queue.size()-1;
        while(!queue.isEmpty()){
            ordered[i--] = queue.poll();
        }
        return ordered;
    }

    public static void main(String[] args) {
        int[] deck = {17,13,11,2,3,5,7};
        int ordered[] = deckRevealedIncreasing(deck);
        
        System.out.print("[");
        for(int num: ordered) {
            System.out.print(num+",");
        }
        System.out.print("]");

    }
}
