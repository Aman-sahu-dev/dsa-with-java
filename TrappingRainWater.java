public class TrappingRainWater {
    public static int trap(int[] height) {
        int water =0;
        int left =0, right = height.length - 1;
        int leftmax = 0 , rightmax = 0;

        while(left <= right){
            leftmax = Math.max(leftmax, height[left]);
            rightmax = Math.max(rightmax, height[right]);

            if(leftmax < rightmax){
                water += leftmax- height[left++];
            }
            else{
                water += rightmax- height[right--];
            }
        }
        return water;
    }
    public static void main(String[] args) {
        int height[] = {0,1,0,2,1,0,1,3,2,1,2,1};
        System.out.println(trap(height));
    }
}
