public class CompareVersionNumbers {
    public static int compareVersion(String version1, String version2) {
        String[] v1 = version1.split("\\.");
        String[] v2 = version2.split("\\.");
        int ans = 0;
        int i=0;
        int j=0;
        int m = v1.length;
        int n = v2.length;
        System.out.println("m: " + m+ ",n: " + n);
        while(i<m && j<n){
            int r1 = Integer.parseInt(v1[i++]);
            int r2 = Integer.parseInt(v2[j++]);
            System.out.println("r1:"+ r1);
            System.out.println("r2:"+ r2);
            if(r1 == r2) continue;
            else if(r1 < r2){
                ans = -1;
                break;
            } 
            else{
                ans =  1;
                break;
            }
        }
        if(i==m && j==n) return ans;
        if(m<n && ans==0){
            while(j<n){
                if(Integer.parseInt(v2[j++])==0){
                    ans = 0;
                    continue;
                }
                else{
                    ans =-1;
                    break;
                }
            }
        }else if(m>n && ans==0){
            while(i<m){
                if(Integer.parseInt(v1[i++])==0){
                    ans = 0;
                    continue;
                }
                else{
                    ans =1;
                    break;
                }
            }
        }

        return ans;
    }
    public static void main(String[] args) {
        // System.out.println(compareVersion("1", "1.0.1"));
        // System.out.println(compareVersion("1.01", "1.001"));
        System.out.println(compareVersion("0.1", "1.1"));
        // System.out.println(compareVersion("1.0", "1.0.0"));
        // System.out.println(compareVersion("1.0.1", "1"));
    }
}
