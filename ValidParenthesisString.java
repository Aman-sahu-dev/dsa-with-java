public class ValidParenthesisString {
    public static boolean checkValidString(String s) {
        int oc = 0, cc =0;
        int n = s.length();
        for(int i = 0; i <n; i++) {
            if(s.charAt(i)=='(' || s.charAt(i)=='*') oc++;
            else oc--;

            if(s.charAt(n-1-i)==')' || s.charAt(n-1-i)=='*') cc++;
            else cc--;

            if(oc<0 || cc<0) return false;
        }
        return true;
    }
    public static void main(String[] args) {
        System.out.println(checkValidString("(((((*(()((((*((**(((()()*)()()()*((((**)())*)*)))))))(())(()))())((*()()(((()((()*(())*(()**)()(())"));
    }
    
}
