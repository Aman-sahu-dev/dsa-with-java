public class FirstPalindromicString {
    public static boolean palindrome(String word){
        int i=0;
        int j=word.length()-1;

        while(i<=j){
            if(word.charAt(i++) == word.charAt(j--))
                continue;
            else
                return false;
        }
        return true;
    }
    public static String getFirstPallindrome(String[] words){
        for(int i = 0; i < words.length;i++){
            if(palindrome(words[i])){
                return words[i];
            }
        }
        return "";
    }
    public static void main(String[] args) {
        String[] words = {"abc","car","ada","racecar","cool"};
        System.out.println(getFirstPallindrome(words));
    }
}
