import java.util.Arrays;

public class BagOfTokens {
    public static int bagOfTokensScore(int[] tokens, int power) {
        Arrays.sort(tokens);
        int l = 0, r = tokens.length-1;
        int score = 0, maxScore = 0;

        while(l<=r){    
            if(power >= tokens[l]){
                power -= tokens[l++];
                score++;
                maxScore = Math.max(maxScore,score);
            }
            else if(score>0){
                power += tokens[r--];
                score--;
            }
            else{
                break;
            }
        }
        return maxScore;
    }
    public static void main(String[] args) {
        int[] tokens = {100,200,300,400};
        int power = 200;
        System.out.println(bagOfTokensScore(tokens, power));
    }
}
