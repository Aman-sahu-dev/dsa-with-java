public class MissingNumber {
    public static int missingNumber(int[] nums){
        int sum=0;
        int result = 0;

        for(int num:nums)
            sum+=num;

        result = sum-nums.length+1;   
        return result;
    }
    public static void main(String[] args) {
        int nums[] = {0,3,1};
        System.out.println(missingNumber(nums));
    }
}
