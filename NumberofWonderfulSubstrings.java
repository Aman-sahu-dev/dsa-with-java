import java.util.HashMap;

public class NumberofWonderfulSubstrings {
    public static long wonderfulSubstrings(String word) {
        HashMap<Long,Long> map = new HashMap<Long,Long>();
        long result =0;
        map.put(Long.valueOf(0), Long.valueOf(1));

        for(char c : word.toCharArray()){
            int shift = 1<<(c - 'a');
            if(map.containsKey((shift))) result+=map.get(shift);
            else map.put(Long.valueOf(shift),map.get(shift)+Long.valueOf(1));
        }
        System.out.println(map);
        return result;
    }
    
    public static void main(String[] args) {
        String word = "aabb";
        // String word = "ehaehcjjaafjdceac";
        System.out.println("Result: "+ wonderfulSubstrings(word));
    }
}
