public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode() {
    }

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }

    public static void printTree(TreeNode root){
        System.out.print(root.val+" -> ");
        if(root.left == null && root.right == null) return;
        if(root.left!=null)
            printTree(root.left);
        if(root.right!=null)
            printTree(root.right);
    }
}
