import java.util.Arrays;

public class FindPolygonWithLargestPerimeter {

    public static void printArray(int[] array){
        for(int num: array){
            System.out.print(num+ " ");
        }
        System.out.println();
    }
    

    public static long findPolygon(int[] nums){
        Arrays.sort(nums);
        int total =0;
        int n = nums.length-1;
        for(int num: nums)        
            total+=num;

        System.out.println("Total: "+ total);
        for(int i=n;i>=2;i--){
            System.out.println("nums[i]: "+ nums[i] + " , total-nums[i]: "+ (total-nums[i]));
            if(total-nums[i] >nums[i]) return total;
            total-=nums[i];
            System.out.println("total-nums[i]: " + total);
        }
        return -1;
    } 
    public static void main(String[] args) {
        int[] nums = {1,12,1,2,5,50,3};
        // int[] nums = {5,5,50};
        // int[] nums = {5,5,5};
        // int[] nums = {1,2,1};
        // int[] nums = {300005055,352368231,311935527,315829776,327065463,388851949,319541150,397875604,311309167,391897750,366860048,359976490,325522439,390648914,359891976,369105322,350430086,398592583,354559219,372400239,344759294,379931363,308829137,335032174,336962933,380797651,378305476,336617902,393487098,301391791,394314232,387440261,316040738,388074503,396614889,331609633,374723367,380418460,349845809,318514711,308782485,308291996,375362898,397542455,397628325,392446446,368662132,378781533,372327607,378737987};
        System.out.println("Result is: "+findPolygon(nums));
    }
}
