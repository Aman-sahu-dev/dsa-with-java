import java.util.Stack;

public class RemoveKDigits{
    public static String removeKdigits(String num, int k) {
        Stack<Character> s = new Stack<Character>();
        StringBuilder ans = new StringBuilder();
        for(int i = 0; i < num.length(); i++){
            char c = num.charAt(i);
            while(!s.isEmpty() && s.peek() > c && k>0){
                s.pop();
                k--;
            }
            s.push(c);
        }
        while(k>0 && !s.isEmpty()){
            s.pop();
            k--;
        }
        while(!s.isEmpty()) {
            ans.append(s.pop());
        }
        if(ans.length()==0) return "0";
        int endingIndex = 0;

        for(int i=ans.length()-1 ;i>=0;i--){
            if(ans.charAt(i)!='0'){
                endingIndex = i;
                break;
            }
        }
        String result = ans.substring(0,endingIndex+1);
        ans = new StringBuilder(result);
        return ans.reverse().toString();
    }
    public static void main(String[] args) {
        System.out.println(removeKdigits("1432219", 3));
        System.out.println(removeKdigits("10200", 1));
        System.out.println(removeKdigits("10", 2));
        System.out.println(removeKdigits("112", 1));
        System.out.println(removeKdigits("10001", 4));
    }
}