public class DistributeCoinsinBinaryTree {
    int moves = 0;
    public static int distributeCoins(TreeNode root) {
        if(root == null) return 0;
        DistributeCoinsinBinaryTree obj = new DistributeCoinsinBinaryTree();
        dfs(root, obj);
        return obj.moves;
    }
    static int dfs(TreeNode root,DistributeCoinsinBinaryTree obj){
        if(root== null) return 0;
        
        int leftrequired = dfs(root.left,obj);
        int rightrequired = dfs(root.right,obj);

        obj.moves += Math.abs(rightrequired)+ Math.abs(leftrequired);
        return (root.val-1) + leftrequired + rightrequired;
    }
    public static void main(String[] args) {
        TreeNode root = new TreeNode(3,new TreeNode(0),new TreeNode(0));
        System.out.println(distributeCoins(root));
    }
}
