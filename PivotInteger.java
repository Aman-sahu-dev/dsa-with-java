public class PivotInteger {
    public static int pivotInteger(int n) {
        int i=1,j=n;
        int frontsum=0;
        int backsum=0;
        if(n==1)  return 1;
        while(i<j){
            System.out.println("frontsum="+frontsum+", backsum="+backsum);
            if(frontsum<backsum)
                frontsum+=i++;
            else backsum+=j--;
            if(frontsum==backsum && i==j)
                return i;
        }
        return -1;
    }
    public static void main(String[] args) {
        int n =15;
        System.out.println(pivotInteger(n));
    }
}
