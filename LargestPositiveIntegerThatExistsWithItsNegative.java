import java.util.Arrays;

public class LargestPositiveIntegerThatExistsWithItsNegative {
    public static int findMaxK(int[] nums) {
        int result = -1;
        Arrays.sort(nums);
        int i=0,j=nums.length-1;

        for(int k:nums) System.out.print(k+", ");
        System.out.println();

        while(i<j && nums[i]<0 && nums[j]>=0){
            if(Math.abs(nums[i])==Math.abs(nums[j])){
                System.out.println("if "+nums[i]+", i: "+ i+", j: "+ j);
                result = Math.max(result,Math.abs(nums[i]));
                i++; 
                j--;
            }
            else if(Math.abs(nums[i])>Math.abs(nums[j])) i++;
            else j--;
        }
        return result;
    }
    public static void main(String[] args) {
        // int[] nums = {-1,10,6,7,-7,1};
        // int nums[] ={-10,8,6,7,-2,-3};
        int[] nums = {-104,-449,-318,-930,-195,579,-410,822,-814,-388,-863,174,-814,919,-877,993,741,741,-623,-4,-4,542,997,239,447,-317,409,-487,-34,6,-914,607,-622,915,573,666,-229,165,841,-820,703};
        System.out.println("result"+ findMaxK(nums));
    }
}
