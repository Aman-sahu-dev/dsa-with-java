import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class FindAllDuplicates {
    public static List<Integer> findDuplicates(int[] nums) {
        List<Integer> result = new ArrayList<>();
        HashSet<Integer> duplicates = new HashSet<>();
        for(int num:nums) {
            if(!duplicates.contains(num))
                duplicates.add(num);
            else{
                result.add(num);
            }
        }
        return result;
    }
    public static void main(String[] args) {
        int[] nums = {4,3,2,7,8,2,3,1};
        List<Integer> list = findDuplicates(nums);
    }
}
