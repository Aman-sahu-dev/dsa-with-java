public class RearrangeArrayElementsBySign {
    public static int[] getRearrageArray(int[] nums) {
        int[] ans = new int[nums.length];
        int pos=0;
        int neg = 1;

        for(int i = 0; i < nums.length; i++) {
            if(nums[i]>0){
                ans[pos] = nums[i];
                pos+=2;
            }
            else{
                ans[neg] = nums[i];
                neg+=2;
            }
        }
        return ans;

    }

    public static void main(String[] args) {
        int[] nums = { 28, -41, 22, -8, -37, 46, 35, -9, 18, -6, 19, -26, -37, -10, -9, 15, 14, 31 };
        int[] result = getRearrageArray(nums);
        for (int num : result) {
            System.out.print(num + " ");
        }
    }
}
