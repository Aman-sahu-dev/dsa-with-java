public class Armstrong {
    public static boolean isArmstrong(int num){
        int result = 0;
        int dup = num;
        while(num!=0){
            int ld = num%10;
            result += (ld*ld*ld);
            num/=10;
            System.out.println("result: "+ result);
        }
        if(result==dup){
            return true;
        }
        return false;
    }
    public static void main(String[] args) {
        System.out.println(isArmstrong(371));
    }
}
