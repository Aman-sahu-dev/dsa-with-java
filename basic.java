

import java.util.Scanner;

public class basic {
    public static void main(String[] args) {

        // Input output
        Scanner scanner = new Scanner(System.in);
        // System.out.println("Hello World");
        // String input = scanner.nextLine();
        // System.out.println("Output is : " + input);
        // scanner.close();

        // DataType in java
        // Two types: Primitive datatype and Reference datatype

        // 1. Primitive datatype

        // byte b = 127; // Range: -128 to 127
        // short s = 20; // Range: -32,768 to 32,767
        // int i = 10; // Range: -2^31 to 2^31 - 1 Size: 32 bits
        // long l = 30L; // Range: -2^63 to 2^63 - 1 Size: 64 bits
        // float f = 3.14f; // Size: 32 bits
        // double d = 40.0; //Size: 64 bits
        // char c= 'a'; //Size: 16 bits
        // boolean bool = false;

        // 2. Reference datatype
        // String str = "aman sahu";
        // int[] myArray = {1, 2, 3, 4, 5};
        // enum Days { SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY }

        // System.out.println(i+ " "+ s + " " + l + " " + d + " " + str + " "+ b+ " " +
        // c + " " + f + " "+ bool);

        // if -else if - else
        // System.out.println("Enter the age: ");
        // int age = scanner.nextInt();
        // if(age<= 0)
        // System.out.println("Invalid age: ");
        // else if(age < 18)
        // System.out.println("You are not an adult");
        // else{
        // System.out.println("You are an adult");
        // }

        // Question
        // System.out.println("Enter the marks: ");
        // int marks = scanner.nextInt();

        // if (marks < 25)
        //     System.out.println("F");
        // else if (marks <= 44)
        //     System.out.println("E");
        // else if (marks <= 49)
        //     System.out.println("D");
        // else if (marks <= 59)
        //     System.out.println("C");
        // else if (marks <= 79)
        //     System.out.println("B");
        // else if (marks <= 100)
        //     System.out.println("A");
        // else
        //     System.out.println("Invalid number");


        // Switch statement
        // System.out.println("Enter the day number: ");
        // int day = scanner.nextInt();

        // switch(day){
        //     case 1: System.out.println("Monday");
        //             break;
        //     case 2: System.out.println("Tuesday");
        //             break;
        //     case 3: System.out.println("Wednesday");
        //             break;
        //     case 4: System.out.println("Thursday");
        //             break;
        //     case 5: System.out.println("Friday");
        //             break;
        //     case 6: System.out.println("Saturday");
        //             break;
        //     case 7: System.out.println("Sunday");
        //             break;
        //     default: System.out.println("Not a valid day");
        // }

        //Arrays 
        // int arr[] = new int[10];
        // System.out.println(arr[9]); // default value in empty is 0.
        // for(int i=0; i<arr.length; i++)
        //     arr[i] = i*2;
        // for(int i=0; i<arr.length; i++)
        //     System.out.print(arr[i]+ " ");

        // Strings
        // String name = "aman";
        // for(int i=0; i<name.length(); i++)
        //     System.out.print(name.charAt(i)+ " ");

        // for loop
        // for(int i=1; i<=10;i++)
        //     System.out.println("2X"+ i+"="+ 2*i);
    }
}
